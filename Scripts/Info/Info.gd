extends Control


# описание видимости блоков Header
const HEADER_SETUP: Dictionary = {
	"is_header_visible": true,
	"is_score_visible": true,
	"is_level_visible": true,
	"is_btn_back_visible": true,
	"is_bg_visible": true,
}

var payload: Dictionary = {} setget set_payload


# BUILTINS -------------------------


# METHODS -------------------------


func get_articles() -> Dictionary:
	var file: File = File.new()
	var articles: Dictionary = {}
	if file.open("res://Scripts/info.json", File.READ) == OK:
		articles = JSON.parse(file.get_as_text()).get("result")
	file.close()
	return articles


func prepare_view(article: int) -> void:
	var articles: Dictionary = get_articles()
	if articles.has("articles"):
		($Scroll/Margin/Text as Label).text = get_articles().articles[article - 1]


# SETGET -------------------------


func set_payload(dict: Dictionary) -> void:
	payload = dict
	#prepare_view(payload.article)


# SIGNALS -------------------------


