extends Control


signal change_view_requested


# описание видимости блоков Header
const HEADER_SETUP: Dictionary = {
	"is_header_visible": true,
	"is_score_visible": true,
	"is_level_visible": true,
	"is_btn_back_visible": true,
	"is_bg_visible": true,
}

var payload: Dictionary = {} setget set_payload

const TOP_OFFSET: float = 0.0
var boxC: BoxContainer
var page: int = 1 setget set_page
var page_count: int = 0
var button_max_w: float = 0.0


const TWEEN_SPEED: float = 0.2
var _t: bool


# BUILTINS -------------------------


func _notification(what: int) -> void:
	if what == NOTIFICATION_RESIZED and boxC != null:
		adjust_pages()
		self.page = page


# METHODS -------------------------


func adjust_pages() -> void:
	var _view: Vector2 = get_viewport_rect().size
	for i in boxC.get_children():
		if  i.rect_size.x > button_max_w:
			button_max_w = i.rect_size.x
	boxC.set("custom_constants/separation", (_view.x - button_max_w) / 2.0)
	boxC.rect_position.y = (_view.y - boxC.rect_size.y) / 2.0 + TOP_OFFSET
	boxC.rect_size.x = 0.0


func prepare_view(level: int) -> void:
	match level:
		1:
			boxC = $Box1
			($Box1 as BoxContainer).show()
			($Box2 as BoxContainer).hide()
			($Box3 as BoxContainer).hide()
		2:
			boxC = $Box2
			($Box1 as BoxContainer).hide()
			($Box2 as BoxContainer).show()
			($Box3 as BoxContainer).hide()
		3:
			boxC = $Box3
			($Box1 as BoxContainer).hide()
			($Box2 as BoxContainer).hide()
			($Box3 as BoxContainer).show()
	adjust_pages()
	page_count = boxC.get_child_count()
	self.page = page


# SETGET -------------------------


func set_payload(dict: Dictionary) -> void:
	payload = dict
	prepare_view(payload.level)


func set_page(value: int) -> void:
	page = value
	var _index: int = value - 1
	var _view_w: float = get_viewport_rect().size.x
	var _new_pos: float = _view_w / 2.0 - button_max_w / 2.0 - button_max_w * _index - boxC.get("custom_constants/separation") * _index
	_t = ($Tween as Tween).interpolate_property(boxC, "rect_position:x", null, _new_pos, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# SIGNALS -------------------------


func _on_BtnLvl1_pressed() -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").SLOT_GAME, { "level": 1 })


func _on_BtnLvl2_pressed() -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").SLOT_GAME, { "level": 2 })


func _on_BtnLvl3_pressed() -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").SLOT_GAME, { "level": 3 })


func _on_BtnLvl4_pressed() -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").SLOT_GAME, { "level": 4 })


func _on_BtnLvl5_pressed() -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").SLOT_GAME, { "level": 5 })


func _on_BtnLvl6_pressed() -> void:
	emit_signal("change_view_requested", Global.get("VIEW_MAP").SLOT_GAME, { "level": 6 })


func _on_BtnLeft_pressed() -> void:
	if not ($Tween as Tween).is_active():
		self.page = page - 1 if page > 1 else page_count


func _on_BtnRight_pressed() -> void:
	if not ($Tween as Tween).is_active():
		self.page = page + 1 if page < page_count else 1


