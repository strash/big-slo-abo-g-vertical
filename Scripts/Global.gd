extends Node


# енум имен сцен
enum VIEW_MAP {
	MENU,                 # 0 - экран основного меню
	SLOT_GAME,            # 1 - экран игры слотов
	SLOT_LEVELS,          # 2 - экран с выбором уровней (слотов/рулетки)
	SLOT_LEVELS_W_PAGING, # 3 - экран с выбором уровней (слотов/рулетки) с горизотальной пагинацией
	ROULETTE_GAME,        # 4 - экран игры рулетки
	AVIATOR_GAME,         # 5 - экран игры авиатор
	INFO,                 # 6 - экран с текстовой информацией
}
# имена всех сцен
const VIEWS: PoolStringArray = PoolStringArray([
	"res://Scenes/MainMenu.tscn",
	"res://Scenes/Slot/SlotGame.tscn",
	"res://Scenes/Slot/Levels.tscn",
	"res://Scenes/Slot/LevelsWPaging.tscn",
	"res://Scenes/Roulette/RouletteGame.tscn",
	"res://Scenes/Aviator/AviatorGame.tscn",
	"res://Scenes/Info/Info.tscn",
])

var REGEX: RegEx = RegEx.new()
var _regex_string: String = "(\\d)(?=(\\d\\d\\d)+([^\\d]|$))"
var _is_regex_ok: bool = false

const DELIMITER: String = ","


# BUILTINS -------------------------


func _init() -> void:
	_is_regex_ok = true if REGEX.compile(_regex_string) == OK else false


# METHODS -------------------------


# форматирование числа, проставление знаков между порядками
func format_number(number: float, delimiter: String = DELIMITER) -> String:
	var num_string: String = str(number)
	if _is_regex_ok:
		return REGEX.sub(num_string, "$1" + delimiter, true)
	else:
		return num_string


# добавление нулей для флоата
func add_zeroes(number: float, delimiter: String = ".") -> String:
	var decimal_index: int = step_decimals(number)
	match decimal_index:
		0:
			return delimiter + "00"
		1:
			return "0" if int(number * 100) % 10 == 0 else ""
		_:
			return ""


